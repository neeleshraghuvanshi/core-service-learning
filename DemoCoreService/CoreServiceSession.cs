﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Tridion.ContentManager.CoreService.Client;

namespace DemoCoreService
{
    public class CoreServiceSession : IDisposable
    {
        private readonly CoreServiceClient _client;
        private const string ServiceUrl = "http://{0}/webservices/CoreService201501.svc/basicHttp";
        private const int MaxMessageSize = 10485760;

        public CoreServiceSession(string hostname)
        {
            _client = CreateBasicHttpClient(hostname);
        }

        public CoreServiceSession(string hostname, string username, string password)
        {
            _client = CreateBasicHttpClient(hostname);

            _client.ChannelFactory.Credentials.Windows.ClientCredential =
                new System.Net.NetworkCredential(username, password);
        }

        private CoreServiceClient CreateBasicHttpClient(string hostname)
        {
            var basicHttpBinding = new BasicHttpBinding
            {
                MaxReceivedMessageSize = MaxMessageSize,
                ReaderQuotas = new XmlDictionaryReaderQuotas
                {
                    MaxStringContentLength = MaxMessageSize,
                    MaxArrayLength = MaxMessageSize
                },
                Security = new BasicHttpSecurity
                {
                    Mode = BasicHttpSecurityMode.TransportCredentialOnly,
                    Transport = new HttpTransportSecurity
                    {
                        ClientCredentialType = HttpClientCredentialType.Windows
                    }
                }
            };
            var remoteAddress = new EndpointAddress(string.Format(ServiceUrl, hostname));
            return new CoreServiceClient(basicHttpBinding, remoteAddress); ;
        }

        public void Dispose()
        {
            if (_client.State == CommunicationState.Faulted)
            {
                _client.Abort();
            }
            else
            {
                _client.Close();
            }
        }
        

        public UserData GetCurrentUser()
        {
            return _client.GetCurrentUser();
        }

        public PublishTransactionData[] Publish(string[] itemUris, PublishInstructionData publishInstructionData,
            string[] destinationTargetUris, PublishPriority publishPriority, ReadOptions readOptions)
        {
            return _client.Publish(itemUris, publishInstructionData, destinationTargetUris, publishPriority, readOptions);
        }
        public string[] GetAllUser()
        {
            var userFilter = new UsersFilterData
            {
                BaseColumns = ListBaseColumns.IdAndTitle,
                IsPredefined = false,
                ItemType = ItemType.User
            };

           var  users = _client.GetSystemWideList(userFilter);
           var userIds = users.Select(f => f.Title).Distinct().ToArray();

            return userIds;
        }

        public List<string> GetAllPages()
        {
            var itemTypes = new List<ItemType>();
            itemTypes.Add(ItemType.Page);

            var filter = new RepositoryItemsFilterData();
            filter.Recursive = true;
            filter.IncludeRelativeWebDavUrlColumn = true;
            filter.ItemTypes = itemTypes.ToArray();

            var pageListXml = _client.GetListXml("tcm:0-129-1", filter);

            List<string> pageTitles = new List<string>();
            foreach (XElement node in pageListXml.Nodes())
            {
                pageTitles.Add(node.Attribute("Title").ToString());
            }

            return pageTitles;
        }

        public void GetUsedItems()
        {
            var itemTypes = new List<ItemType>();
            itemTypes.Add(ItemType.Page);

            var filter = new RepositoryItemsFilterData();
            filter.Recursive = true;
            filter.IncludeRelativeWebDavUrlColumn = true;
            filter.ItemTypes = itemTypes.ToArray();
            filter.BaseColumns = ListBaseColumns.Extended;
            List<KeyValuePair<string, string>> listOfCp = new List<KeyValuePair<string, string>>();
            listOfCp.Add(new KeyValuePair<string, string>("2844", "34433"));
            listOfCp.Add(new KeyValuePair<string, string>("3123", "34433"));
            listOfCp.Add(new KeyValuePair<string, string>("2844", "34457"));
            listOfCp.Add(new KeyValuePair<string, string>("2983", "34461"));
            listOfCp.Add(new KeyValuePair<string, string>("2983", "34381"));
            listOfCp.Add(new KeyValuePair<string, string>("2983", "34409"));
            listOfCp.Add(new KeyValuePair<string, string>("3123", "34425"));
            listOfCp.Add(new KeyValuePair<string, string>("2925", "34474"));
            listOfCp.Add(new KeyValuePair<string, string>("2952", "34426"));


            List<string> tcmids = new List<string>() { "147","240","308", "243","152", "245", "437","439","452", "254", "258", "277", "256", "283", "284", "260", "278", "279", "252", "280", "242", "250", "241", "281", "290", "291", "264", "239", "262", "282", "285", "155", "158", "266", "244", "154", "246", "247", "289" };


            foreach (var tcm in tcmids)
            {

                string filename = String.Format("{0}__info", tcm);

                var filePath = string.Format(@"E:\componentfiles\{0}.txt", filename);

                if (!File.Exists(filePath))
                {

                    using (StreamWriter file = File.CreateText(filePath))
                    {
                        file.WriteLine(string.Format("Script running starts for publication {0}", tcm));
                        IEnumerable<PageData> pages = _client.GetList(string.Format("tcm:0-{0}-1", tcm), filter).Cast<PageData>();

                        foreach (var page in pages)
                        {
                            PageData pageData = (PageData)_client.Read(page.Id, null);
                            if (pageData.ComponentPresentations!=null && pageData.ComponentPresentations.Any())
                            {
                                foreach (ComponentPresentationData cp in pageData.ComponentPresentations)
                                {
                                    
                                    ComponentData cmpData = (ComponentData)_client.Read(cp.Component.IdRef, null);
                                    String[] splitSchemaID = cmpData.Schema.IdRef.Split('-');
                                    String[] splitComponentTemplateID = cp.ComponentTemplate.IdRef.Split('-');
                                    
                                   
                                    if (listOfCp.Contains((new KeyValuePair<string, string>(splitSchemaID[1], splitComponentTemplateID[1]))))
                                    {
                                       
                                            ComponentData cmp = (ComponentData)_client.Read(cp.Component.IdRef, null);
                                            SchemaFieldsData schemaFields = _client.ReadSchemaFields(cmp.Schema.IdRef, true, null);
                                            ItemFieldDefinitionData compMetaData = schemaFields.MetadataFields.FirstOrDefault();

                                            if (compMetaData is EmbeddedSchemaFieldDefinitionData)
                                            {
                                                XDocument content = XDocument.Parse(cmp.Metadata);
                                                XNamespace ns = schemaFields.NamespaceUri;


                                                // print out the subfields of the Address (e.g. Street, City, PostCode)
                                                foreach (XElement field in content.Root.Element(ns + "page_metadata").Elements())
                                                {
                                                    if (field.Name.LocalName == "PageNameOverride")
                                                    {
                                                        file.WriteLine(string.Format("page Name Override Value found component {0} with schema {1}", cmp.Id, cmp.Schema.Title));
                                                        file.WriteLine(string.Format("page tcm is  {0}, page title {1} and ct is {2}", page.Id, page.Title, cp.ComponentTemplate.Title));
                                                        file.WriteLine();

                                                    }
                                                }
                                            }
                                           
                                      }
                                }
                            }
                            
                        }
                    }
                }
            }
        }
         

        public void GetTemplates()
        {
            var itemTypes = new List<ItemType>();
            itemTypes.Add(ItemType.ComponentTemplate);

            var filter = new OrganizationalItemItemsFilterData();
            filter.ItemTypes = itemTypes.ToArray();
            var filePath = string.Format(@"E:\ctinfo\ctinfonew.txt");
            IEnumerable<ComponentTemplateData> ctData = _client.GetList("tcm:147-1317-2", filter).Cast<ComponentTemplateData>();
            using (StreamWriter file = new StreamWriter(filePath))
            {
                foreach (var ctItem in ctData)
                {
                    bool found = true;
                    ComponentTemplateData ct = (ComponentTemplateData)_client.Read(ctItem.Id, null);
                    if (ct.MetadataSchema.Title == "Component Template Metadata")
                    {
                        XElement content = XElement.Parse(ct.Metadata);
                        IEnumerable<XElement> allChildElements = content.Elements();

                        foreach (var item in allChildElements)
                        {
                            if (item.Name.LocalName == "localizationreq" && item.Value=="Yes")
                            {
                                found = false;
                                break;
                            }
                        }
                    }
                    if (found)
                    {
                        LinkToSchemaData[] relatedSchemas = ct.RelatedSchemas;

                        foreach (LinkToSchemaData linkSchema in relatedSchemas)
                        {
                            
                            SchemaFieldsData schemaFields = _client.ReadSchemaFields(linkSchema.IdRef, true, null);
                            ItemFieldDefinitionData compMetaData = schemaFields.MetadataFields.FirstOrDefault();

                            
                            if (compMetaData!= null && compMetaData is EmbeddedSchemaFieldDefinitionData && compMetaData.Name =="page_metadata")
                            {
                                

                                file.WriteLine(String.Format("Found Schema wchich has metadata attached {0} with id {1}", linkSchema.Title, linkSchema.IdRef));
                                file.WriteLine(String.Format("Found CT for localization req not set {0} and tcm is {1}", ct.Title, ct.Id));
                                file.WriteLine();

                                
                            }

                        }
                       
                        
                       
                    }
                       

                }
                file.Close();
            }
            
        }

        public List<string> GetAllDefaultPages()
        {
            var itemTypes = new List<ItemType>();
            itemTypes.Add(ItemType.Page);

            var filter = new RepositoryItemsFilterData();
            filter.Recursive = true;
            filter.IncludeRelativeWebDavUrlColumn = true;
            filter.ItemTypes = itemTypes.ToArray();
            filter.BaseColumns = ListBaseColumns.Extended;

            List<string> tcmids = new List<string>() {"157", "254", "258", "277", "256", "283", "284", "260", "278", "279", "252", "280", "242", "250", "241", "281", "290", "291", "264", "239", "262", "282", "285", "155", "158", "266", "244", "154", "246", "247", "289" };
            

            foreach (var tcm in tcmids)
            {
                int numberOfDefaultPages = 0;
                int numberOfCorrection = 0;
                string filename = String.Format("{0}__info", tcm);
                
                var filePath = string.Format(@"E:\files\{0}.txt", filename);

                if (!File.Exists(filePath))
                {
                    
                    using (StreamWriter file = File.CreateText(filePath))
                    {
                        file.WriteLine(string.Format("Script running starts for publication {0}", tcm));
                        IEnumerable<PageData> pages = _client.GetList(string.Format("tcm:0-{0}-1", tcm), filter).Cast<PageData>();

                        foreach (var page in pages)
                        {
                            if (page.FileName == "default")
                            {
                                numberOfDefaultPages = numberOfDefaultPages + 1;
                                PageData pageData = (PageData)_client.Read(page.Id, null);
                                if (pageData.ComponentPresentations != null && pageData.ComponentPresentations.Any())
                                {
                                    foreach (var item in pageData.ComponentPresentations)
                                    {
                                        ComponentData firstComponent = (ComponentData)_client.Read(item.Component.IdRef, null);
                                        SchemaFieldsData schemaFields = _client.ReadSchemaFields(firstComponent.Schema.IdRef, true, null);
                                        ItemFieldDefinitionData compMetaData = schemaFields.MetadataFields.FirstOrDefault();

                                        if (compMetaData is EmbeddedSchemaFieldDefinitionData)
                                        {
                                            XDocument content = XDocument.Parse(firstComponent.Metadata);
                                            XNamespace ns = schemaFields.NamespaceUri;


                                            // print out the subfields of the Address (e.g. Street, City, PostCode)
                                            foreach (XElement field in content.Root.Element(ns + "page_metadata").Elements())
                                            {
                                                if (field.Name.LocalName == "PageNameOverride")
                                                {
                                                    numberOfCorrection = numberOfCorrection + 1;

                                                    file.WriteLine(string.Format("page Name Override Value {0} and page tcm is {1}", field.Value, page.Id));
                                                    file.WriteLine();

                                                }
                                            }
                                        }
                                    }


                                }

                            }

                        }
                        file.WriteLine(string.Format("total number of default pages scanned  : {0}", numberOfDefaultPages));
                        file.WriteLine();
                        file.WriteLine(string.Format("total number changes required  : {0} ", numberOfCorrection));
                        file.WriteLine();
                        file.WriteLine(string.Format("Script running Ends for publication {0}", tcm));
                        file.Close();
                    }

                }
               
                
                
            }
            
            
           
            List<string> pageTitles = new List<string>();
            

            return pageTitles;
        }



    }
}
